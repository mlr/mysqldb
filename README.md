# Overview

The MySQL DB Abstraction class is a simple wrapper for PHP's mysqli database functions. It allows for automatic prepared statements with parameter binding and result fetching. This means that you do not have to manually bind result records to individual variables. The library will pull results and create an associative array of results for you, without regard to the number of fields being fetched.

# Usage

First, import the file into your project. Then require it:

	require_once('mysql.class.php');
	
Next, initialize an instance of the class, passing your database credentials to the constructor:

	$db = new DB('hostname', 'username', 'password', 'database');
	
### Simple queries

This library allows you to chain method calls to your instance of the class. For example, to run a query and fetch the results, you'd do:

	$records = $db->query("SELECT * FROM `table`")->fetch();	// returns an array of results
	print_r($records);
	
### Creating a prepared statement

To create a prepared statement, just pass an SQL query to the *query()* method, replacing values to be prepped/sanitized with a question mark.
Pass values for each question mark in an array as the second parameter. Like so:

	$records = $db->query("SELECT * FROM `table` WHERE `field1` = ? OR `field2` = ?", array("Value 1", "Value 2"))->fetch();
	
If you only have one prepared statement placeholder, you can pass it as a regular argument, rather than an array. The library will take care of it for you.

	$records = $db->query("SELECT * FROM `table` WHERE `id` = ?", 15)->fetch();
	
### Insert/Update queries

For insertions or updates, you'll need to use the *execute()* method after your query:

	// Insert a row
	$new_id = $db->query("INSERT INTO `table` (`id`, `name`) VALUES(?, ?)", array(NULL, "Ronnie Miller"))->execute();
	echo $new_id; // gives the ID of the new record.
	
	// Update a row
	$db->query("UPDATE `table` SET `name` = ? WHERE `id` = ?", array("Ron Miller", $new_id))->execute();

### Single result records

If you want to fetch just one record from the db, pass the row number of the record you want to the fetch method.

	$person = $db->query("SELECT * FROM `table`")->fetch(0);	// Fetches the first record
	$person = $db->query("SELECT * FROM `table`")->fetch(4);	// Fetches the 5th record

You can also use the *result()* method, which is simply an alias of fetch. The difference is, result defaults to the first record.

	$person = $db->query("SELECT * FROM `table`")->result();	// Fetches the first record

### Single result fields

When using the library, if you only give one column in your SELECT statement and there is only one record in the result, you'll be returned only that value, not an array. This is useful for quickly grabbing individual column values from a record.

	$name = $db->query("SELECT `name` FROM `table` WHERE `id` = ?", 15)->result();